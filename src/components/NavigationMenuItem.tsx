import React from 'react';
import './NavigationMenuItem.css';

type Props = {
  title: string;
}

export default class NavigationMenuItem extends React.Component<Props> {
  render() {
    return(
      <div>
        {this.props.title}
      </div>
    )
  }
}