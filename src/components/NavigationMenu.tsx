import React from 'react';
import NavigationMenuItem from './NavigationMenuItem';

type Props = {
  
}

const NavigationMenu: React.FC<Props> = () => {
  return (
    <div>
      <NavigationMenuItem title="Home" />
      <NavigationMenuItem title="Sign in" />
      <NavigationMenuItem title="Search" />
      <NavigationMenuItem title="Tv Shows" />
    </div>
  );
}

export default NavigationMenu